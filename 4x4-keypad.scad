/*
    4 x 4 keypad 65 x 64 mm
    PA5DOF
*/

/*
    A single key
    
    10.4 x 9.4mm
    
    @todo: size is a bit off, should be 10.1 x 9.1
*/
module key() {
    union() {
        cube([10.4,9.4, 0.03], center=true);
        translate([0,0,1.15]) cube([8.7,7.7, 2.3], center=true);
    }       
}    

/*
    bottom part 65 x 64 mm
*/
module keypad_bottom() {
    color("Black", 1.0)  hull(){
        translate([4,4,0]) cylinder(h=3, r1=4, r2=4, center=true, $fn=24);
        translate([61,4,0]) cylinder(h=3, r1=4, r2=4, center=true, $fn=24);
        translate([4,60,0]) cylinder(h=3, r1=4, r2=4, center=true, $fn=24);
        translate([61,60,0]) cylinder(h=3, r1=4, r2=4, center=true, $fn=24);
    }
}    

/*
    center part 60 x 57 mm
*/
module keypad_center() {
    color("Black", 1.0) hull(){
        translate([5,6,3]) cylinder(h=3.6, r1=2.5, r2=2.5, center=true, $fn=24);
        translate([60,6,3]) cylinder(h=3.6, r1=2.5, r2=2.5, center=true, $fn=24);
        translate([5,58,3]) cylinder(h=3.6, r1=2.5, r2=2.5, center=true, $fn=24);
        translate([60,58,3]) cylinder(h=3.6, r1=2.5, r2=2.5, center=true, $fn=24);    
        
    }
}    

/*
    keypad pcb 5 x 52.4 x 1 mm
*/
module keypad_pcb() {
    color("DarkGreen", 1.0) translate([6.3,-5,-1.5]) cube([52.4,5,1]);
}    

/*
    Main module
*/
module keypad_4x4() {
 
    difference() {
            union() {
                keypad_bottom();     
                keypad_center();
                
                // 4 x 4 keys
                color("DarkSlateGray", 1.0) translate([11.5,12.5,0]) for (col=[0:3]) for (row=[0:3]) translate([row *(10.4+3.3),col*(9.4+3.6),4.8]) key();
                
                keypad_pcb();   
            }    
            union() {
                // 4 x mounting holes 2.3mm diameter
                translate([2.5,2.5,0]) cylinder(h=5, r1=1.3, r2=1.3, center=true, $fn=24);
                translate([62.5,2.5,0]) cylinder(h=5, r1=1.3, r2=1.3, center=true, $fn=24);
                translate([2.5,61.5,0]) cylinder(h=5, r1=1.3, r2=1.3, center=true, $fn=24);
                translate([62.5,61.5,0]) cylinder(h=5, r1=1.3, r2=1.3, center=true, $fn=24);
            }    
    }    
}    

keypad_4x4();